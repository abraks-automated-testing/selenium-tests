# Preparation

Install Maven in your local environment if not yet available.

# Test Running

The all tests can be run by typing the command
`mvn test`

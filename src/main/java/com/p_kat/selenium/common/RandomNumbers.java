package com.p_kat.selenium.common;

import java.util.Random;

public class RandomNumbers {
    Random rand = new Random();

    public Integer generateNumber() {
        return rand.nextInt(100000);
    }
}

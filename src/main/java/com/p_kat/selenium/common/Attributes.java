package com.p_kat.selenium.common;

public class Attributes {
    public static final Integer TIMEOUT_SECONDS = 10;
    public static final Integer POLL_M_SECONDS = 300;
    public static final String CONFIRMATION_UPDATE_MESSAGE = "Rekord został pomyślnie zaktualizowany.";
    public static final String CONFIRMATION_REMOVAL_MESSAGE = "Rejestr został usunięty.";
    private Attributes() {

    }
}

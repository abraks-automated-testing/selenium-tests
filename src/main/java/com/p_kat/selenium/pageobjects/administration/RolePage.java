package com.p_kat.selenium.pageobjects.administration;

import com.p_kat.selenium.pageobjects.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RolePage extends PageObject {
    @FindBy(id = "admin_role_manage")
    private WebElement roleForm;
    @FindBy(id = "display-name")
    private WebElement inputName;
    @FindBy(id = "role-description")
    private WebElement inputDescription;
    @FindBy(id = "deleteButton")
    private WebElement deleteButton;

    public RolePage(WebDriver driver) {
        super(driver);
    }

    public RolePage isInitialized() {
        roleForm.isDisplayed();
        return this;
    }

    public RolePage insertName(String name) {
        inputName.sendKeys(name);
        return this;
    }

    public void insertDescription(String text) {
        inputDescription.sendKeys(text);
    }

    public RolePage clickRemoveIcon(String roleName) {
        removeIconElement(roleName).click();
        return this;
    }

    public void confirmRemoval() {
        deleteButton.isDisplayed();
        deleteButton.click();
    }

    private WebElement removeIconElement(String roleName) {
        return driver.findElement(By.xpath(String.format("//tr/td[normalize-space(text())='%s']/..//a[contains(@class, 'deleteStepOne')]", roleName)));
    }
}
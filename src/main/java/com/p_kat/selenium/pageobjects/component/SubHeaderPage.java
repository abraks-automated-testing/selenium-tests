package com.p_kat.selenium.pageobjects.component;

import com.p_kat.selenium.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SubHeaderPage extends PageObject {
    @FindBy(id = "addModelButton_1")
    private WebElement addButton;

    public SubHeaderPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddButton() {
        addButton.click();
    }
}

package com.p_kat.selenium.pageobjects.component;

import com.p_kat.selenium.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FormPage extends PageObject {
    @FindBy(name = "submit")
    private WebElement submitButton;

    public FormPage(WebDriver driver) {
        super(driver);
    }

    public void clickSubmitButton() {
        submitButton.click();
    }
}

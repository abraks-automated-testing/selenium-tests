package com.p_kat.selenium.pageobjects.component;

import com.p_kat.selenium.common.Attributes;
import com.p_kat.selenium.pageobjects.PageObject;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class AssertionPage extends PageObject {
    @FindBy(className = "alert-success")
    private WebElement successMessage;
    @FindBy(xpath = "//i[contains(@class, 'fa-times')]")
    private WebElement closeIcon;
    @FindBy(className = "disappearing")
    private WebElement successBox;

    public AssertionPage(WebDriver driver) {
        super(driver);
    }

    public String getConfirmationMessage() {
        return successMessage.getText().trim();
    }

    public void closeConfirmationMessage() {
        closeIcon.click();
    }

    public void waitSuccessBoxIsVisible() {
        Wait<WebDriver> wait =
                new FluentWait<>(driver)
                        .withTimeout(Duration.ofSeconds(Attributes.TIMEOUT_SECONDS))
                        .pollingEvery(Duration.ofMillis(Attributes.POLL_M_SECONDS))
                        .ignoring(ElementNotInteractableException.class);
        wait.until(
                d -> {
                    successBox.isDisplayed();
                    return true;
                });
    }
}

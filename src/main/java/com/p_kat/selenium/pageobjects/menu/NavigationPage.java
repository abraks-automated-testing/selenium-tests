package com.p_kat.selenium.pageobjects.menu;

import com.p_kat.selenium.common.Menu;
import com.p_kat.selenium.pageobjects.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavigationPage extends PageObject {
    public NavigationPage(WebDriver driver) {
        super(driver);
    }

    public NavigationPage expandMenuForAdministration() {
        expandMenuFor(Menu.ADMINISTRATION);
        return this;
    }

    public void clickMenuForRoles() {
        clickMenuFor(Menu.ROLES);
    }

    public void clickMenuFor(String linkName) {
        navigationElement(linkName).click();
    }

    public void expandMenuFor(String linkName) {
        navigationElement(linkName).click();
    }

    private WebElement navigationElement(String linkText) {
        return driver.findElement(By.xpath(String.format("//a[@title='%s']", linkText)));
    }
}

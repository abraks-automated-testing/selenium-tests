package com.p_kat.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends PageObject {
    @FindBy(id = "panel-lista-projektow")
    private WebElement projectsPanel;
    @FindBy(id = "panel-lista-srodowisk")
    private WebElement environmentsPanel;

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public DashboardPage isProjectsPanelVisible() {
        projectsPanel.isDisplayed();
        return this;
    }

    public void isEnvironmentsPanelVisible() {
        environmentsPanel.isDisplayed();
    }
}

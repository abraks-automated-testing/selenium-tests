package com.p_kat.selenium.functional;

import com.p_kat.selenium.BaseTest;
import com.p_kat.selenium.administration.Role;
import com.p_kat.selenium.common.Dashboard;
import com.p_kat.selenium.common.Login;
import com.p_kat.selenium.common.menu.Navigation;
import com.p_kat.selenium.system.PropertyReader;
import org.testng.annotations.Test;

public class AdministrationTest extends BaseTest {

    @Test
    public void accessDashboard() {
        driver.get(PropertyReader.getProperty("applicationUrl"));
        Login login = new Login();
        Dashboard dashboard = new Dashboard();
        login.loginAsAdmin(driver);
        dashboard.assertProjectsPanel(driver);
    }

    @Test
    public void assignRoleForCurrentUser() {
        Navigation navigation = new Navigation();
        navigation.goToRoles(driver);
        Role role = new Role();
        role.addNewRole(driver);
        role.removeRole(driver);
    }
}

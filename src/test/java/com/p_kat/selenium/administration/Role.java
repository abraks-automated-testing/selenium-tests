package com.p_kat.selenium.administration;

import com.p_kat.selenium.common.RandomNumbers;
import com.p_kat.selenium.component.Assertion;
import com.p_kat.selenium.pageobjects.administration.RolePage;
import com.p_kat.selenium.pageobjects.component.FormPage;
import com.p_kat.selenium.pageobjects.component.SubHeaderPage;
import org.openqa.selenium.WebDriver;

public class Role {

    private String roleName = "";
    private String roleDescription = "";

    public void addNewRole(WebDriver driver) {
        RandomNumbers randomNumbers = new RandomNumbers();
        int randomNumber = randomNumbers.generateNumber();
        SubHeaderPage subHeaderPage = new SubHeaderPage(driver);
        subHeaderPage.clickAddButton();
        RolePage rolePage = new RolePage(driver);
        setRoleName("Rola " + randomNumber);
        setRoleDescription("Opis " + randomNumber);
        rolePage.isInitialized().insertName(getRoleName()).insertDescription(getRoleDescription());
        FormPage formPage = new FormPage(driver);
        formPage.clickSubmitButton();
        Assertion assertion = new Assertion();
        assertion.assertUpdateMessage(driver);
        assertion.closeAssertedMessage(driver);
    }

    public void removeRole(WebDriver driver) {
        RolePage rolePage = new RolePage(driver);
        rolePage.clickRemoveIcon(getRoleName()).confirmRemoval();

        Assertion assertion = new Assertion();
        assertion.assertRemovalMessage(driver);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }
}

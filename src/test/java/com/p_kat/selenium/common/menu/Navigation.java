package com.p_kat.selenium.common.menu;

import com.p_kat.selenium.pageobjects.menu.NavigationPage;
import org.openqa.selenium.WebDriver;

public class Navigation {

    public void goToRoles(WebDriver driver) {
        NavigationPage navigationPage = new NavigationPage(driver);
        navigationPage.expandMenuForAdministration().clickMenuForRoles();
    }
}

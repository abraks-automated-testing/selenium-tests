package com.p_kat.selenium.common;

import com.p_kat.selenium.pageobjects.LoginPage;
import com.p_kat.selenium.system.PropertyReader;
import org.openqa.selenium.WebDriver;

public class Login {
    public static final String ADMIN_EMAIL = PropertyReader.getProperty("adminEmail");
    public static final String ADMIN_PASSWORD = PropertyReader.getProperty("adminPassword");

    public void loginAsAdmin(WebDriver driver) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.isInitialized().insertEmail(ADMIN_EMAIL).insertPassword(ADMIN_PASSWORD).clickLoginButton();
    }

}

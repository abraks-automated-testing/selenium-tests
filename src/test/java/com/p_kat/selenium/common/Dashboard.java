package com.p_kat.selenium.common;

import com.p_kat.selenium.pageobjects.DashboardPage;
import org.openqa.selenium.WebDriver;

public class Dashboard {

    public void assertProjectsPanel(WebDriver driver) {
        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.isProjectsPanelVisible().isEnvironmentsPanelVisible();
    }
}

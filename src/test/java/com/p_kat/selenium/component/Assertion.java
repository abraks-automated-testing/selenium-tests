package com.p_kat.selenium.component;

import com.p_kat.selenium.common.Attributes;
import com.p_kat.selenium.pageobjects.component.AssertionPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class Assertion {

    public void assertUpdateMessage(WebDriver driver) {
        AssertionPage assertionPage = new AssertionPage(driver);
        Assert.assertEquals(assertionPage.getConfirmationMessage(), Attributes.CONFIRMATION_UPDATE_MESSAGE);
    }

    public void assertRemovalMessage(WebDriver driver) {
        AssertionPage assertionPage = new AssertionPage(driver);
        assertionPage.waitSuccessBoxIsVisible();
        Assert.assertEquals(assertionPage.getConfirmationMessage(), Attributes.CONFIRMATION_REMOVAL_MESSAGE);
    }

    public void closeAssertedMessage(WebDriver driver) {
        AssertionPage assertionPage = new AssertionPage(driver);
        assertionPage.waitSuccessBoxIsVisible();
        assertionPage.closeConfirmationMessage();
    }
}
